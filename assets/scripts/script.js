let btnCompute = document.getElementById('btnCompute');
let divComputation = document.getElementById('divComputation');
let friends = [];
let friends2 = friends;
btnCompute.addEventListener('click',function(){
	let bill = document.getElementById('txtBill').value;
	let txtFriend = document.getElementById('txtFriend').value;
	addFriend(txtFriend);
	showFriends();
});

function addFriend(name){
	friends.push(name);
}

function showFriends(){
	$('.displayFriends').remove();
	let bill = document.getElementById('txtBill').value;
	let shares = bill/(friends.length);
	shares = shares.toFixed(2);
	let newContainer = document.createElement('div');
	newContainer.classList.add('displayFriends');
	divComputation.appendChild(newContainer, divComputation.firstElementChild);
	friends.forEach(function(friend){
		let newFriend = document.createElement('h6');
		let newShares = document.createElement('span');
		newShares.classList.add('newShare');
		newFriend.textContent = friend;
		newShares.textContent = " "+shares;
		document.querySelector('.displayFriends').appendChild(newFriend, document.querySelector('.displayFriends').firstElementChild);
		newFriend.insertBefore(newShares, newFriend.firstElementChild);
	});

}


// function showFriends(){
// 	let bill = document.getElementById('txtBill').value;
// 	let shares = bill/(friends.length);


// 	friends.forEach(function(friend){
// 		let newFriend = document.createElement('h6');
// 		let newShares = document.createElement('span');
// 		newShares.classList.add('newShare');
// 		newFriend.textContent = friend;
// 		newShares.textContent = " "+shares;
// 		displayFriends.appendChild(newFriend, displayFriends.firstElementChild);
// 		newFriend.insertBefore(newShares, newFriend.firstElementChild);
// 	});

// }
